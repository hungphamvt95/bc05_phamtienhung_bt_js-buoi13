const tiGiaUSD = 23500;
function quyDoiTien() {
  var soTienQuyDoi = document.getElementById("txt-so-tien-quy-doi").value * 1;
  var tienDoi = soTienQuyDoi * tiGiaUSD;
  var tienDoiFormat = new Intl.NumberFormat("de-DE", {
    style: "currency",
    currency: "VND",
  }).format(tienDoi);
  document.getElementById("result").innerHTML = tienDoiFormat;
}

