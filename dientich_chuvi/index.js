function tinhChuViDienTich() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;
  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) * 2;
  document.getElementById(
    "result"
  ).innerHTML = `Chu vi hình chữ nhật là: ${chuVi} <br> Diện tích hình chữ nhật là: ${dienTich}`;
}
